package itis.parsing;

import itis.parsing.annotations.FieldName;
import itis.parsing.annotations.MaxLength;
import itis.parsing.annotations.NotBlank;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ParkParsingServiceImpl implements ParkParsingService {

    //Парсит файл в обьект класса "Park", либо бросает исключение с информацией об ошибках обработки
    @Override
    public Park parseParkData(String parkDatafilePath) throws ParkParsingException {

        Park park = null;
        Constructor<Park> constructor = null;
        try {
            constructor =  Park.class.getDeclaredConstructor();
            constructor.setAccessible(true);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        try {
            assert constructor != null;
            park = constructor.newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        File file = new File(parkDatafilePath);
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        List<ParkParsingException.ParkValidationError> parkValidationErrors = new ArrayList<>();
        while (true){
            assert scanner != null;
            if (!scanner.hasNextLine()) break;
            String line = scanner.nextLine();
            if (!line.equals("***")){
                String fieldName = line.substring(1, line.indexOf(":") - 1);
                String value = line.split(":")[1].replaceAll("\"", "").trim();
                if (value.equals("null")){
                    value = null;
                }
                Field field = null;
                for (Field field1: Park.class.getDeclaredFields()) {
                    if (field1.isAnnotationPresent(FieldName.class)){
                        if (field1.getAnnotation(FieldName.class).value().equals(fieldName)){
                            field = field1;
                            break;
                        }
                    } else if (field1.getName().equals(fieldName)){
                        field = field1;
                        break;
                    }
//                    Annotation fieldNameAnnotation = field.getAnnotation(FieldName.class);
//                    Annotation maxLength = field.getAnnotation(MaxLength.class);
//                    Annotation notBlank = field.getAnnotation(NotBlank.class);
                }

                if (field == null){
                    continue;
                }
                field.setAccessible(true);
                try {
                    boolean brakeData = false;
                    if (field.isAnnotationPresent(MaxLength.class)){
                        assert value != null;
                        if (value.length() > field.getAnnotation(MaxLength.class).value()){
                            parkValidationErrors.add(new ParkParsingException.ParkValidationError(fieldName,
                                    "Превышена максимальная длина поля"));
                            brakeData = true;
                        }
                    }
                    if (field.isAnnotationPresent(NotBlank.class)){
                        if (value == null || value.equals("")){
                            parkValidationErrors.add(new ParkParsingException.ParkValidationError(fieldName,
                                    "Данные отсутствуют"));
                            brakeData = true;
                        }
                    }
                    if (brakeData){
                        continue;
                    }
                    if (field.getType() == Integer.class){
                        assert value != null;
                        field.set(park, Integer.parseInt(value));
                    } else if (field.getType() == LocalDate.class){
                        assert value != null;
                        field.set(park, LocalDate.parse(value));
                    } else {
                        field.set(park, value);
                    }

                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        if (parkValidationErrors.isEmpty()) {
            return park;
        } else {
            throw new IllegalArgumentException(new ParkParsingException("Ошибки в данных", parkValidationErrors));
        }
    }
}
